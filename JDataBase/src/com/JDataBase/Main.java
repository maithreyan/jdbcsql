package com.JDataBase;
import javax.print.DocFlavor;
import java.sql.*;

public class Main {

    public static void main(String[] args) throws Exception
    {
        String url="jdbc:mysql://localhost:3306/student";
        String usn="root";
        String pwd="adventurE@28";
        String query="select * from info ";
	//Class.forName("com.mysql.jdbc.Driver");     //class forname search to understand the concept
        Connection con= DriverManager.getConnection(url,usn,pwd);
        Statement st=con.createStatement();
        //st.executeQuery(query);
        ResultSet rs=st.executeQuery(query);

        while(rs.next()) {
            String userdata = rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4);

            System.out.println(userdata);
        }
        st.close();
        con.close();
    }
}
